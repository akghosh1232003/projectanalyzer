package com.metlife.starteam;

public class ProjectAnalyzerConst {
	
	public static final String JAR_EXTN = "jar";
	public static final String EXCEL_TEMPLATE_NAME = "StarTeamProjectInventory.xls";
	public static final String PROJECT_INVENTORY_EXCEL_TEMPLATE = "src/main/resources/StarTeamProjectInventory.xls";
	
}
